#!/usr/bin/env python
import argparse
import os.path as op
import os
import logging
from jinja2 import Environment, FileSystemLoader
import numpy as np
logger = logging.getLogger(__name__)


def run(n_crossing, n_params, overwrite=False):
    """
    Generates a CUDIMOT model to fit a multi-Bingham distribution

    :param n_crossing: number of crossing fibres
    :param n_params: number of degrees of variation (e.g., TE, b-value, diffusion time)
    :param overwrite: if True, overwrites already existing directory
    """
    template_dir = op.split(op.abspath(__file__))[0]
    parent_directory = op.split(template_dir)[0]
    directory_name = f'{parent_directory}/Bingham_{n_crossing}_cross_{n_params}_var'
    if op.isdir(directory_name):
        if not overwrite:
            logging.warning('Output directory %s already exists; set --overwrite to overwrite it', directory_name)
            return
    else:
        os.mkdir(directory_name)

    total_params = n_crossing * 3 * (n_params + 2)
    init = np.zeros(total_params, dtype='i4')
    init[3::(3 * (n_params + 2))] = 3
    init[4 + n_params::(3 * (n_params + 2))] = 3
    init[5 + 2 * n_params::(3 * (n_params + 2))] = 3
    init = ','.join([str(v) for v in init])

    env = Environment(
            loader=FileSystemLoader(template_dir),
    )
    for filename in (
        'modelfunctions.h',
        'modelparameters.cc',
        'modelparameters.h',
        'modelpriors'
    ):
        template = env.get_template(filename + '.jinja2')
        text = template.render(
                n_params=n_params,
                n_crossing=n_crossing,
                init=init
        )
        with open(op.join(directory_name, filename), 'w') as f:
            f.write(text)


def run_from_args(args):
    """
    Runs the script based on a Namespace containing the command line arguments
    """
    run(
            args.n_crossing,
            args.n_params,
            overwrite=args.overwrite,
    )


def get_parser(parser="Generates a CUDIMOT model to fit a multi-Bingham distribution"):
    """
    Creates the parser of the command line arguments
    """
    if isinstance(parser, str):
        parser = argparse.ArgumentParser(parser)
    parser.add_argument('n_crossing', type=int, help='number of crossing fibres')
    parser.add_argument('n_params', type=int, help='number of degrees of variation (e.g., TE, b-value, diffusion time)')
    parser.add_argument('-o', '--overwrite', action='store_true', help='overwrites already existing directory')
    return parser


def main():
    """
    Runs the script from the command line
    """
    parser = get_parser()

    args = parser.parse_args()

    run_from_args(args)


if __name__ == "__main__":
    main()